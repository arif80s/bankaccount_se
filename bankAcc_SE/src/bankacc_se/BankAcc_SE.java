/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankacc_se;

import com.jsonObject.JsonObjectProvider;
import com.model.UserAccountModel;
import com.request.RequestSend;
import java.io.IOException;
import static java.lang.System.in;
import java.util.Scanner;

/**
 *
 * @author Arif
 */
public class BankAcc_SE {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(in);
        System.out.println("welcome to Bank Applications!!\n");
        System.out.println("Give input 1 through 9\n");
        int operation = sc.nextInt();
        if(operation == 1)
        {
            System.out.println("creating an account.......\n");
            System.out.println("Give Account holder Name : ");
            String acc_holder_name = sc.next();
            System.out.println("Give Account holder Adress : ");
            String acc_address = sc.next();
            System.out.println("give account creation date : ");
            String acc_Creation_Date = sc.next( );
            System.out.println("Initial Balances : ");
            int acc_balance = sc.nextInt();
            System.out.println("Nominee info : ");
            String accnomineeInfo = sc.next();
            
            UserAccountModel userAccountModel = new UserAccountModel(acc_holder_name, acc_address, acc_Creation_Date, acc_balance, accnomineeInfo);
            JsonObjectProvider jsonObj = new JsonObjectProvider();
            String jsonStringAcc = jsonObj.createJsonStringAccount(userAccountModel);
            //System.out.println("json String : " + jsonStringAcc);
            RequestSend req  = new RequestSend();
            if(req.sendPostRequest(jsonStringAcc) == 200)
            {
                System.out.println("account created successfully!!!");
            }
            
            
            
        }
        
    }
    
}
