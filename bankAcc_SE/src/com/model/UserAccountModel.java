/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author Arif
 */
public class UserAccountModel {
    //private int acc_Number;
    private String acc_holder_name;
    private String acc_address;
    private String acc_Creation_Date;
    private int acc_balance;
    private String accnomineeInfo;

    public UserAccountModel() {
    }
    public UserAccountModel(String acc_holder_name, String acc_address, String acc_Creation_Date, int acc_balance, String accnomineeInfo) {
        this.acc_holder_name = acc_holder_name;
        this.acc_address = acc_address;
        this.acc_Creation_Date = acc_Creation_Date;
        this.acc_balance = acc_balance;
        this.accnomineeInfo = accnomineeInfo;
    }


    public String getAcc_holder_name() {
        return acc_holder_name;
    }

    public void setAcc_holder_name(String acc_holder_name) {
        this.acc_holder_name = acc_holder_name;
    }

    public String getAcc_address() {
        return acc_address;
    }

    public void setAcc_address(String acc_address) {
        this.acc_address = acc_address;
    }

    public String getAcc_Creation_Date() {
        return acc_Creation_Date;
    }

    public void setAcc_Creation_Date(String acc_Creation_Date) {
        this.acc_Creation_Date = acc_Creation_Date;
    }

    public int getAcc_balance() {
        return acc_balance;
    }

    public void setAcc_balance(int acc_balance) {
        this.acc_balance = acc_balance;
    }

    public String getAccnomineeInfo() {
        return accnomineeInfo;
    }

    public void setAccnomineeInfo(String accnomineeInfo) {
        this.accnomineeInfo = accnomineeInfo;
    }

}
