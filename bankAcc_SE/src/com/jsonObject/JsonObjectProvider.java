/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsonObject;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.UserAccountModel;
import java.io.IOException;

/**
 *
 * @author Arif
 */
public class JsonObjectProvider {
        String jsonInString;

//  return json formatting object
    public String createJsonStringAccount(UserAccountModel user) 
    {
        ObjectMapper mapper = new ObjectMapper();

        try 
        {
//          make json formatting string
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);

        } 
        catch (JsonGenerationException e) 
        {
        } 
        catch (JsonMappingException e) 
        {
        } 
        catch (IOException e) 
        {
        }
        
        return jsonInString;
    
    }
}
