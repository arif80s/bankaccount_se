/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.request;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Arif
 */
public class RequestSend {
    public int sendPostRequest(String jsonString) throws MalformedURLException, IOException
    {
        
        URL url = new URL("http://localhost:8080/bank/createAcc");
        
//      create connection to the url
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        
        connection.setConnectTimeout(5000);//5 secs
        connection.setReadTimeout(5000);//5 secs
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        
//      make outputstream writer with connection to send the post request        
        try (OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream())) 
        {
            out.write(jsonString);
            out.flush();
        }

        int response = connection.getResponseCode();
        
//      finally close the connection
        connection.disconnect();
        
        return response;

    }
}
